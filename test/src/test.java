import com.mumei.jpuny.JPunyException;
import com.mumei.jpuny.PunyDomain;

/**
 * Created by @mumei_himazin on 2014/04/26.
 */
public class test {
	public static void main(String[] arg){

		try {
			System.out.println("test:ておくれ.の.みんな---------------------------");
			String ress = PunyDomain.encode("ておくれ.の.みんな");
			System.out.println(ress);
			System.out.println(PunyDomain.decode(ress));
		} catch (JPunyException e) {
			e.printStackTrace();
		}
		System.out.print("\n");

		try {
			System.out.println("test:test.com---------------------------");
			String ress = PunyDomain.encode("test.com");
			System.out.println(ress);
			System.out.println(PunyDomain.decode(ress));
		} catch (JPunyException e) {
			e.printStackTrace();
		}
		System.out.print("\n");

		try {
			System.out.println("test:こっち.jp.みんな---------------------------");
			String ress = PunyDomain.encode("こっち.jp.みんな");
			System.out.println(ress);
			System.out.println(PunyDomain.decode(ress));
		} catch (JPunyException e) {
			e.printStackTrace();
		}
		System.out.print("\n");

		try {
			System.out.println("test:3年b組.com---------------------------");
			String ress = PunyDomain.encode("3年b組.com");
			System.out.println(ress);
			System.out.println(PunyDomain.decode(ress));
		} catch (JPunyException e) {
			e.printStackTrace();
		}
		System.out.print("\n");

		try {
			System.out.println("test:xn--t8jk1evh.xn--u9j.xn--q9jyb4c---------------------------");
			String ress = PunyDomain.encode("xn--t8jk1evh.xn--u9j.xn--q9jyb4c");
			System.out.println(ress);
			System.out.println(PunyDomain.decode(ress));
		} catch (JPunyException e) {
			e.printStackTrace();
		}
		System.out.print("\n");
	}
}

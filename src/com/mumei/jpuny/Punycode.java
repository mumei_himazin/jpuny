package com.mumei.jpuny;

import java.util.TreeSet;

/**
 * Created by @mumei_himazin on 2014/04/26.
 */
public class Punycode {

	final static int TMIN			= 1;
	final static int TMAX			= 26;
	final static int BASE			= 36;
	final static int INITIAL_N		= 128;
	final static int INITIAL_BIAS	= 72;
	final static int DAMP			= 700;
	final static int SKEW			= 38;
	final static char DELIMITER		= '-';

	public static String encode(String input) throws JPunyException{
		return encode(input,"");
	}

	public static String encode(String input,String flag) throws JPunyException {

		StringBuilder output = new StringBuilder();

		TreeSet<Character> nbCodepoints =new TreeSet<Character>();

		for(int i=0;i<input.length();i++){
			char c = input.charAt(i);
			if(c<0x80){
				output.append(c);
			}else{
				nbCodepoints.add(c);
			}
		}

		if(nbCodepoints.isEmpty()){
			return input;
		}

		int n = INITIAL_N;
		int bias = INITIAL_BIAS;
		int delta = 0;

		int h = output.length();
		final int b = h;

		output.insert(0,flag);

		if(h>0)output.append(DELIMITER);

		for(Character m:nbCodepoints){

			if((m-n)>(Integer.MAX_VALUE-delta)/(h+1))
				throw new JPunyException(JPunyException.OVERFLOW);

			delta += (m - n) * (h+1);
			n = m;
			for(int i =0;i<input.length();i++){
				final char c = input.charAt(i);
				if(c<n){
					++delta;
					if(delta==0)
						throw new JPunyException(JPunyException.OVERFLOW);
				}else if(c==n){
					int q = delta;
					for(int k= BASE;;k+=BASE){
						int t = k<=bias? TMIN: k>=bias+TMAX? TMAX:k-bias;
						if(q<t)break;
						output.append(encodeChar( t+(q-t)%(BASE-t) ));
						q = (q-t)/(BASE-t);
					}
					output.append(encodeChar(q));
					bias = adapt(delta,h+1,h==b);
					delta = 0;
					++h;
				}
			}
			++delta;
			n++;
		}
		return output.toString();
	}


	public static String decode(String input) throws JPunyException {
		StringBuilder output = new StringBuilder();
		int d;
		if((d=input.lastIndexOf(DELIMITER))>0){
			String base = input.substring(0,d);
			if(!base.matches("[0-9a-zA-Z]*"))
				throw new JPunyException(JPunyException.BAD_INPUT);
			output.append(base);
		}
		d++;
		int n = INITIAL_N;
		int bias = INITIAL_BIAS;
		int i = 0;

		for(;d<input.length();d++){
			int oldi = i;
			int w =1;
			for(int k= BASE;;k+=BASE){
				if (d == input.length())
					throw new JPunyException(JPunyException.BAD_INPUT);
				char c = input.charAt(d);
				int digit = encodeInt(c);
				if(digit > (Integer.MAX_VALUE-i)/w)
					throw new JPunyException(JPunyException.OVERFLOW);
				i+= (digit*w);

				int t = k<=bias? TMIN: k>=bias+TMAX? TMAX:k-bias;

				if( digit< t )break;
				w *= (BASE-t);
				d++;
			}
			final int nextLength = output.length()+1;
			bias = adapt(i-oldi,nextLength,oldi==0);
			if(i/nextLength>Integer.MAX_VALUE-n)
				throw new JPunyException(JPunyException.OVERFLOW);
			n+=(i/nextLength);
			i%=nextLength;
			char ch = (char)n;
			output.insert(i,ch);
			++i;
		}
		return output.toString();
	}

	private static int adapt(int delta,int point,boolean isFirst){
		delta=isFirst?delta/DAMP:delta/2;
		delta+=(delta/point);
		int baseSubMin = (BASE-TMIN);
		int k=0;
		while(delta>(baseSubMin*TMAX)/2){
			delta/=baseSubMin;
			k+=BASE;
		}
		return k+ ((baseSubMin+1)*delta) / (delta+SKEW);
	}

	private static char encodeChar(int d) throws JPunyException {
		if(d>=36 || d<0)throw new JPunyException(JPunyException.BAD_INPUT);
		return (char)(d<26 ? d+'a' : d-26+'0');
	}
	private static int encodeInt(char c) throws JPunyException {
		if(c-'0'<10){
			return c-'0'+26;
		}else if(c-'a'<26){
			return c-'a';
		}
		throw new JPunyException(JPunyException.BAD_INPUT);
	}
}

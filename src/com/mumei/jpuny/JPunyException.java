package com.mumei.jpuny;

/**
 * Created by @mumei_himazin on 2014/04/26.
 */
public class JPunyException extends Exception {
	public static final String OVERFLOW		= "Overfolow";
	public static final String BAD_INPUT	= "bad input";

	public JPunyException(String message){
		super(message);
	}
}

package com.mumei.jpuny;

/**
 * Created by @mumei_himazin on 2014/04/26.
 */
public class PunyDomain {
	private static final String FLAG = "xn--";

	public static String encode(String input) throws JPunyException {
		String[] subs = input.split("\\.");
		StringBuilder output = new StringBuilder();
		int i = 0;
		for(String sub:subs) {
			output.append(Punycode.encode(sub,FLAG));
			if(subs.length-1>i)output.append('.');
			i++;
		}
		return output.toString();
	}

	public static String decode(String input) throws JPunyException {
		String[] subs = input.split("\\.");
		StringBuilder output = new StringBuilder();
		int i = 0;
		for(String sub:subs) {
			if(sub.matches("^"+FLAG+".*$")){
				output.append(Punycode.decode(sub.substring(FLAG.length())));
			}else{
				output.append(sub);
			}
			if(subs.length-1>i)output.append('.');
			i++;
		}
		return output.toString();
	}
}
